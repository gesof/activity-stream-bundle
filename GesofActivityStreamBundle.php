<?php

namespace Gesof\ActivityStreamBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Gesof\ActivityStreamBundle\DependencyInjection\Compiler\ResolverPass;

class GesofActivityStreamBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        
        $container->addCompilerPass(new ResolverPass());
    }
}
