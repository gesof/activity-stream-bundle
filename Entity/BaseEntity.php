<?php

namespace Gesof\ActivityStreamBundle\Entity;

use Symfony\Component\PropertyAccess\PropertyAccess;

class BaseEntity 
{
    protected $propertyAccessor;
    
    public function getPropertyAccessor()
    {
        if (!$this->propertyAccessor) {
            $this->propertyAccessor  = PropertyAccess::createPropertyAccessor();
        }
            
        return $this->propertyAccessor;
    }
    
    public function populate($data)
    {
        $accessor = $this->getPropertyAccessor();
        unset($data['id']);

        foreach ($data as $field => $value) {
            $accessor->setValue($this, $field, $value);
        }
    }
}
