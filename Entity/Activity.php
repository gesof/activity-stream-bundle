<?php

namespace Gesof\ActivityStreamBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Gesof\ActivityStreamBundle\Model\ActivityInterface;
use Doctrine\Common\Util\ClassUtils;

/**
 * @ORM\Entity(repositoryClass="Gesof\ActivityStreamBundle\Repository\ActivityRepository")
 * @ORM\Table(name="gesof_activity_stream__activity")
 * @ORM\HasLifecycleCallbacks
 */
class Activity extends BaseEntity implements ActivityInterface
{
    const ENTITY_ALIAS = 'actv';
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __toString()
    {
        if ($this->hasTarget()) {
            if ($this->hasObject()) {
                return sprintf('%s %s %s on %s %s', $this->getActor(), $this->getVerb(), $this->getObject(), $this->getTarget(), $this->getCreatedAt());
            }
            else {
                return sprintf('%s %s %s', $this->getActor(), $this->getVerb(), $this->getTarget() , $this->getCreatedAt());
            }
        }
        
        return sprintf('%s %s %s', $this->getActor(), $this->getVerb(), $this->getCreatedAt());
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;
    
    /** 
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $package;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $code;

    /** 
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    protected $url;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $verb;
    
    /**
     * Virtual - will be resolved
     */
    protected $actor;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $actor_id;

    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $actor_class;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $actor_type;
    
    /** 
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $actor_data;
    
    /**
     * Virtual - will be resolved
     */
    protected $target;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $target_id;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $target_class;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $target_type;
    
    /** 
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $target_data;

    /**
     * Virtual - will be resolved
     */
    protected $object;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $object_id;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $object_class;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $object_type;
    
    /** 
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $object_data;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_at;
    
    /**
     * @ORM\OneToMany(targetEntity="Gesof\ActivityStreamBundle\Entity\Notification", mappedBy="Activity")
     */
    private $Notifications;
    
    /**
     * @ORM\OneToMany(targetEntity="Gesof\ActivityStreamBundle\Entity\Invitation", mappedBy="Activity")
     */
    private $Invitations;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Notifications = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Invitations = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    protected function extractDisplayName($entity)
    {
        switch (TRUE) {
            case method_exists($entity, 'getFullName'):
                $label = $entity->getFullName();
                break;
            
            case method_exists($entity, 'getName'):
                $label = $entity->getName();
                break;
            
            case method_exists($entity, 'getTitle'):
                $label = $entity->getTitle();
                break;
            
            case method_exists($entity, '__toString'):
                $label = (string) $entity;
                break;
            
            default:
                $label = "";
        }
        
        $label = trim($label);
        
        if (empty($label)) {
            $class = get_class($entity);
            $class = explode( '\\', $class);
            $className = end($class);
            $label = sprintf('%s-%s', $className, $entity->getId());
        }
        
        return $label;
    }
    
    /** 
     * @ORM\PrePersist 
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime();
    }
    
    // Setters & getters
    

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Activity
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Activity
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Set package
     *
     * @param string $package
     *
     * @return Activity
     */
    public function setPackage($package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * Get package
     *
     * @return string
     */
    public function getPackage()
    {
        return $this->package;
    }
    
    /**
     * Set code
     *
     * @param string $code
     *
     * @return Activity
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set url
     *
     * @param string $url
     *
     * @return Activity
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * Set verb
     *
     * @param string $verb
     *
     * @return Activity
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;

        return $this;
    }

    /**
     * Get verb
     *
     * @return string
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * Get actor
     * 
     * @return type
     */
    public function getActor() 
    {
        return $this->actor;
    }

    /**
     * Set actor
     * 
     * @param type $actor
     */
    public function setActor($actor) 
    {
        if ($actor) {
            $this->actor = $actor;
            $this->actor_id = $actor->getId();
            $this->actor_class = $this->getClass($actor);
            $this->actor_data = array(
                'displayName' => $this->extractDisplayName($actor)
            );
        }
        
        return $this;
    }

        /**
     * Set actorId
     *
     * @param integer $actorId
     *
     * @return Activity
     */
    public function setActorId($actorId)
    {
        $this->actor_id = $actorId;

        return $this;
    }

    /**
     * Get actorId
     *
     * @return integer
     */
    public function getActorId()
    {
        return $this->actor_id;
    }

    /**
     * Set actorType
     *
     * @param string $actorClass
     *
     * @return Activity
     */
    public function setActorClass($actorClass)
    {
        $this->actor_class = $actorClass;

        return $this;
    }

    /**
     * Get actorClass
     *
     * @return string
     */
    public function getActorClass()
    {
        return $this->actor_class;
    }

    /**
     * Set actorData
     *
     * @param array $actorData
     *
     * @return Activity
     */
    public function setActorData($actorData)
    {
        $this->actor_data = $actorData;

        return $this;
    }

    /**
     * Get actorData
     *
     * @return array
     */
    public function getActorData()
    {
        return $this->actor_data;
    }

    /**
     * Get target
     * 
     * @return type
     */
    public function getTarget() 
    {
        return $this->target;
    }

    /**
     * 
     * @param type $target
     * @return \Gesof\ActivityStreamBundle\Entity\Activity
     */
    public function setTarget($target) 
    {
        if ($target) {
            $this->target = $target;
            $this->target_id = $target->getId();
            $this->target_class = $this->getClass($target);
            $this->target_data = array(
                'displayName' => $this->extractDisplayName($target)
            );
        }
        
        return $this;
    }

    public function hasTarget()
    {
        return (bool) $this->getTarget();
    }
    
    /**
     * Set targetId
     *
     * @param integer $targetId
     *
     * @return Activity
     */
    public function setTargetId($targetId)
    {
        $this->target_id = $targetId;

        return $this;
    }

    /**
     * Get targetId
     *
     * @return integer
     */
    public function getTargetId()
    {
        return $this->target_id;
    }

    /**
     * Set targetClass
     *
     * @param string $targetClass
     *
     * @return Activity
     */
    public function setTargetClass($targetClass)
    {
        $this->target_class = $targetClass;

        return $this;
    }

    /**
     * Get targetClass
     *
     * @return string
     */
    public function getTargetClass()
    {
        return $this->target_class;
    }

    /**
     * Set targetData
     *
     * @param array $targetData
     *
     * @return Activity
     */
    public function setTargetData($targetData)
    {
        $this->target_data = $targetData;

        return $this;
    }

    /**
     * Get targetData
     *
     * @return array
     */
    public function getTargetData()
    {
        return $this->target_data;
    }

    /**
     * Get object
     * 
     * @return type
     */
    public function getObject() 
    {
        return $this->object;
    }

    /**
     * 
     * @param type $object
     * @return \Gesof\ActivityStreamBundle\Entity\Activity
     */
    public function setObject($object) 
    {
        if ($object) {
            $this->object = $object;
            $this->object_id = $object->getId();
            $this->object_class = $this->getClass($object);
            $this->object_data = array(
                'displayName' => $this->extractDisplayName($object)
            );
        }
        
        return $this;
    }

    public function hasObject()
    {
        return (bool) $this->getObject();
    }
    
    /**
     * Set objectId
     *
     * @param integer $objectId
     *
     * @return Activity
     */
    public function setObjectId($objectId)
    {
        $this->object_id = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return integer
     */
    public function getObjectId()
    {
        return $this->object_id;
    }

    /**
     * Set objectClass
     *
     * @param string $objectClass
     *
     * @return Activity
     */
    public function setObjectClass($objectClass)
    {
        $this->object_class = $objectClass;

        return $this;
    }

    /**
     * Get objectType
     *
     * @return string
     */
    public function getObjectClass()
    {
        return $this->object_class;
    }

    /**
     * Set objectData
     *
     * @param array $objectData
     *
     * @return Activity
     */
    public function setObjectData($objectData)
    {
        $this->object_data = $objectData;

        return $this;
    }

    /**
     * Get objectData
     *
     * @return array
     */
    public function getObjectData()
    {
        return $this->object_data;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Activity
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    protected function getClass($object)
    {
        return is_object($object) ? ClassUtils::getClass($object) : NULL;
            
    }

    /**
     * Add notification
     *
     * @param \Gesof\ActivityStreamBundle\Entity\Notification $notification
     *
     * @return Activity
     */
    public function addNotification(\Gesof\ActivityStreamBundle\Entity\Notification $notification)
    {
        $this->Notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param \Gesof\ActivityStreamBundle\Entity\Notification $notification
     */
    public function removeNotification(\Gesof\ActivityStreamBundle\Entity\Notification $notification)
    {
        $this->Notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotifications()
    {
        return $this->Notifications;
    }

    /**
     * Add invitation
     *
     * @param \Gesof\ActivityStreamBundle\Entity\Invitation $invitation
     *
     * @return Activity
     */
    public function addInvitation(\Gesof\ActivityStreamBundle\Entity\Invitation $invitation)
    {
        $this->Invitations[] = $invitation;

        return $this;
    }

    /**
     * Remove invitation
     *
     * @param \Gesof\ActivityStreamBundle\Entity\Invitation $invitation
     */
    public function removeInvitation(\Gesof\ActivityStreamBundle\Entity\Invitation $invitation)
    {
        $this->Invitations->removeElement($invitation);
    }

    /**
     * Get invitations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvitations()
    {
        return $this->Invitations;
    }

    /**
     * Set actorType
     *
     * @param string $actorType
     *
     * @return Activity
     */
    public function setActorType($actorType)
    {
        $this->actor_type = $actorType;

        return $this;
    }

    /**
     * Get actorType
     *
     * @return string
     */
    public function getActorType()
    {
        return $this->actor_type;
    }

    /**
     * Set targetType
     *
     * @param string $targetType
     *
     * @return Activity
     */
    public function setTargetType($targetType)
    {
        $this->target_type = $targetType;

        return $this;
    }

    /**
     * Get targetType
     *
     * @return string
     */
    public function getTargetType()
    {
        return $this->target_type;
    }

    /**
     * Set objectType
     *
     * @param string $objectType
     *
     * @return Activity
     */
    public function setObjectType($objectType)
    {
        $this->object_type = $objectType;

        return $this;
    }

    /**
     * Get objectType
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }
}
