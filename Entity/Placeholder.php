<?php

namespace Gesof\ActivityStreamBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity(repositoryClass="Gesof\ActivityStreamBundle\Repository\PlaceholderRepository")
 * @ORM\Table(name="gesof_activity_stream__placeholder")
 * @ORM\HasLifecycleCallbacks
 */
class Placeholder 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $displayName;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $objectType;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_at;

    public function __toString()
    {
        return $this->displayName;
    }
    
    /** 
     * @ORM\PrePersist 
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return Placeholder
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set objectType
     *
     * @param string $objectType
     *
     * @return Placeholder
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;

        return $this;
    }

    /**
     * Get objectType
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Placeholder
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Placeholder
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Placeholder
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}
