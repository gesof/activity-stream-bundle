<?php

namespace Gesof\ActivityStreamBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity(repositoryClass="Gesof\ActivityStreamBundle\Repository\InvitationRepository")
 * @ORM\Table(name="gesof_activity_stream__invitation")
 * @ORM\HasLifecycleCallbacks
 */

class Invitation extends BaseEntity
{
    CONST STATUS_PENDING = 'pending';
    CONST STATUS_ACCEPTED = 'accepted';
    CONST STATUS_REJECTED = 'rejected';
    
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /** 
     * Optional: can generate your own at rendering time
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;
    
    /**
     * Optional: can use activity stream code instead
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;
    
    /**
     * Optional: add extra data on some cases
     * 
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $extra;
    
   /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $status;

    /**
     * Use TRUE for non existing users
     * Also use a virtual entity with email
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_annonymous;
    
    /**
     * Notified by email for non registered user ?
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_notified;

    /**
     * Notified by email for non registered user ?
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_deleted;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expires_at;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Gesof\ActivityStreamBundle\Entity\Activity")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $Activity;
    
    // Magick

    public function __construct()
    {
        $this->status = self::STATUS_PENDING;
        $this->is_annonymous = FALSE;
        $this->is_notified = FALSE;
	$this->is_deleted = FALSE;
    }
    
    // Lifecycle Callbacks
    
   /** 
    * @ORM\PrePersist 
    */
    public function onPrePersist()
    {
	$now = new \DateTime();
	
        $this->created_at = $now;
	$this->updated_at = $now;
	
	if (!$this->token) {
	    $this->generateToken();
	}
        
        $expiresAt = clone $now;
        $expiresAt->modify('+2days');
        
        $this->expires_at = $expiresAt;
    }

   /** 
    * @ORM\PreUpdate
    */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime();
    }
    
    // Virtual
    
    protected function generateToken()
    {
	$this->token = sha1(uniqid('ntf'));
	
	return $this;
    }
    
    // Setters & getters
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Invitation
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * Set code
     *
     * @param string $code
     *
     * @return Invitation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set extra
     *
     * @param array $extra
     *
     * @return Invitation
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Invitation
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Invitation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isAnnonymous
     *
     * @param boolean $isAnnonymous
     *
     * @return Invitation
     */
    public function setIsAnnonymous($isAnnonymous)
    {
        $this->is_annonymous = $isAnnonymous;

        return $this;
    }

    /**
     * Get isAnnonymous
     *
     * @return boolean
     */
    public function getIsAnnonymous()
    {
        return $this->is_annonymous;
    }

    /**
     * Set isNotified
     *
     * @param boolean $isNotified
     *
     * @return Invitation
     */
    public function setIsNotified($isNotified)
    {
        $this->is_notified = $isNotified;

        return $this;
    }

    /**
     * Get isNotified
     *
     * @return boolean
     */
    public function getIsNotified()
    {
        return $this->is_notified;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return Invitation
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set expiresAt
     *
     * @param \DateTime $expiresAt
     *
     * @return Invitation
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expires_at = $expiresAt;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expires_at;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Invitation
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Invitation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set activity
     *
     * @param \Gesof\ActivityStreamBundle\Entity\Activity $activity
     *
     * @return Invitation
     */
    public function setActivity(\Gesof\ActivityStreamBundle\Entity\Activity $activity = null)
    {
        $this->Activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \Gesof\ActivityStreamBundle\Entity\Activity
     */
    public function getActivity()
    {
        return $this->Activity;
    }
}
