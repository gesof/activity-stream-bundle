<?php

namespace Gesof\ActivityStreamBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * TODO: RENAME TABLE AFTER DONE
 * 
 * @ORM\Entity(repositoryClass="Gesof\ActivityStreamBundle\Repository\NotificationRepository")
 * @ORM\Table(name="gesof_activity_stream__notification")
 * @ORM\HasLifecycleCallbacks
 */
class Notification extends BaseEntity
{
    const ENTITY_ALIAS = 'ntf';
    
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /** 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;
    
    /** 
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $extra;
    
    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_read;

    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $read_at;
    
    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_pushed;

    /** 
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $pushed_to;
    
    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $pushed_at;

    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_hidden;
    
    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $hidden_at;
    
    /** 
     * This notification is created by the user itself
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_own;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Gesof\ActivityStreamBundle\Entity\Activity")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $Activity;
    
    // Will be mapped dinamycally
//    /** 
//     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
//     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
//     */
    private $User;

    // Magick

    public function __construct()
    {
        $this->is_read = FALSE;
	$this->is_pushed = FALSE;
        $this->is_hidden = FALSE;
    }
    
    // Lifecycle Callbacks
    
   /** 
    * @ORM\PrePersist 
    */
    public function onPrePersist()
    {
	$now = new \DateTime();
	
        $this->created_at = $now;
	$this->updated_at = $now;
	
	if (!$this->token) {
	    $this->generateToken();
	}
    }

   /** 
    * @ORM\PreUpdate
    */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime();
    }
    
    // Virtual
    
    protected function generateToken()
    {
	$this->token = sha1(uniqid('ntf'));
	
	return $this;
    }
    
    // Setters & getters


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Notification
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Notification
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set extra
     *
     * @param array $extra
     *
     * @return Notification
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return Notification
     */
    public function setIsRead($isRead)
    {
        $this->is_read = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function getIsRead()
    {
        return $this->is_read;
    }

    /**
     * Set readAt
     *
     * @param \DateTime $readAt
     *
     * @return Notification
     */
    public function setReadAt($readAt)
    {
        $this->read_at = $readAt;

        return $this;
    }

    /**
     * Get readAt
     *
     * @return \DateTime
     */
    public function getReadAt()
    {
        return $this->read_at;
    }

    /**
     * Set isPushed
     *
     * @param boolean $isPushed
     *
     * @return Notification
     */
    public function setIsPushed($isPushed)
    {
        $this->is_pushed = $isPushed;

        return $this;
    }

    /**
     * Get isPushed
     *
     * @return boolean
     */
    public function getIsPushed()
    {
        return $this->is_pushed;
    }

    /**
     * Set pushedTo
     *
     * @param array $pushedTo
     *
     * @return Notification
     */
    public function setPushedTo($pushedTo)
    {
        $this->pushed_to = $pushedTo;

        return $this;
    }

    /**
     * Get pushedTo
     *
     * @return array
     */
    public function getPushedTo()
    {
        return $this->pushed_to;
    }

    /**
     * Set pushedAt
     *
     * @param \DateTime $pushedAt
     *
     * @return Notification
     */
    public function setPushedAt($pushedAt)
    {
        $this->pushed_at = $pushedAt;

        return $this;
    }

    /**
     * Get pushedAt
     *
     * @return \DateTime
     */
    public function getPushedAt()
    {
        return $this->pushed_at;
    }

    /**
     * Set isHidden
     *
     * @param boolean $isHidden
     *
     * @return Notification
     */
    public function setIsHidden($isHidden)
    {
        $this->is_hidden = $isHidden;

        return $this;
    }

    /**
     * Get isHidden
     *
     * @return boolean
     */
    public function getIsHidden()
    {
        return $this->is_hidden;
    }

    /**
     * Set hiddenAt
     *
     * @param \DateTime $hiddenAt
     *
     * @return Notification
     */
    public function setHiddenAt($hiddenAt)
    {
        $this->hidden_at = $hiddenAt;

        return $this;
    }

    /**
     * Get hiddenAt
     *
     * @return \DateTime
     */
    public function getHiddenAt()
    {
        return $this->hidden_at;
    }

    /**
     * Set isOwn
     *
     * @param boolean $isOwn
     *
     * @return Notification
     */
    public function setIsOwn($isOwn)
    {
        $this->is_own = $isOwn;

        return $this;
    }

    /**
     * Get isOwn
     *
     * @return boolean
     */
    public function getIsOwn()
    {
        return $this->is_own;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Notification
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Notification
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set activity
     *
     * @param \Gesof\ActivityStreamBundle\Entity\Activity $activity
     *
     * @return Notification
     */
    public function setActivity(\Gesof\ActivityStreamBundle\Entity\Activity $activity = null)
    {
        $this->Activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \Gesof\ActivityStreamBundle\Entity\Activity
     */
    public function getActivity()
    {
        return $this->Activity;
    }

    /**
     * Set user
     *
     * @param \FOS\UserBundle\Model\User $user
     *
     * @return Notification
     */
    public function setUser($user)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }
}
