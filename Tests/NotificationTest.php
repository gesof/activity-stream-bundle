<?php

namespace Gesof\ActivityStreamBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Description of NotificationTest
 *
 * @author seby
 */
class NotificationTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     * 
     * @var type 
     */
    protected $container;
    
    /** @var \Gesof\ActivityStreamBundle\Services\NotificationManager */
    protected $notificationManager;
    
    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();
        
        $this->container = static::$kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $this->notificationManager = $this->container->get('gesof_activity_stream.notification_manager');
        
    }
    
    public function testIndex()
    {
        /* @var $repo \Gesof\ActivityStreamBundle\Repository\NotificationRepository */
        $repo = $this->em->getRepository('GesofActivityStreamBundle:Notification');
        
        $qb = $repo->createQB();
        $qb->setMaxResults(10);
        
        $qb2 = clone $qb;
        
        $repo->addWhereIsHidden($qb);
        $repo->addWhereIsNotHidden($qb);
        
        $entities = $qb->getQuery()->getResult();
        $entities2 = $qb2->getQuery()->getResult();
    }
    
    public function testCreate()
    {
        $user = $this->provideUser();
        
        $entity = $this->notificationManager->newInstance();
        $entity->setUser($user);
        
        $this->notificationManager->create($entity);
        
        $this->notificationManager
            ->markAsRead($entity, TRUE)
            ->markAsPushed($entity, TRUE)
            ->markAsHidden($entity, TRUE)
        ;
        
        $this->em->remove($entity);
        $this->em->flush();
    }
    
    protected function provideUser()
    {
        $userClass = $this->container->getParameter('gesof_activity_stream.user_class');
        
        $user = $this->em->getRepository($userClass)->findOneBy(array(), NULL, 1);
        
        return $user;
    }
}
