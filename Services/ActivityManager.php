<?php

namespace Gesof\ActivityStreamBundle\Services;

use Gesof\ActivityStreamBundle\Entity\Activity;
use Gesof\ActivityStreamBundle\Event\ActivityEvent;
use Gesof\ActivityStreamBundle\Events;

/**
 * Service: gesof_activity_stream.activity_manager
 */
class ActivityManager
{
    protected $em;
    protected $eventDispatcher;
    protected $repo;
    
    public function __construct($em, $eventDispatcher)
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
        $this->repo = $em->getRepository('GesofActivityStreamBundle:Activity');
    }
    
    public function findAll()
    {
        return $this->repo->findAll();
    }
    
    // For custom queries
    public function getRepository()
    {
        return $this->repo;
    }

    public function newInstance()
    {
        $entity = new Activity();
        
        return $entity;
    }
    
    /**
     * Create entity using entity or array data
     * 
     * @param type $data
     * @return Activity
     */
    public function create($data)
    {
        if ($data instanceof Activity) {
            $entity = $data;
        }
        else if (is_array($data)) {
            $entity = new Activity();
            $entity->populate($data);
        }
        else {
            throw new \Exception('Invalid data provided. Must be array or instanceof Activity');
        }
        
        $parts = array_filter(array($entity->getActorType(), $entity->getVerb(), $entity->getObjectType(), $entity->getTargetType()));
        $code = implode('.', $parts);
        
        $entity->setCode($code);
        
        $this->em->persist($entity);
        $this->em->flush($entity);
        
        $this->eventDispatcher->dispatch(Events::ACTIVITY_CREATE, new ActivityEvent($entity, $this->em));
        
        return $entity;
    }
    
    /**
     * Find by critteria
     * 
     * @param type $critteria
     * @param type $orderBy
     * @param type $offset
     * @param type $maxResults
     * @return type
     */
    public function findBy($critteria, $orderBy = array('id' => 'DESC'), $offset = 0, $maxResults = 10)
    {
        $entities = $this->repo->getBy($critteria, $orderBy, $offset, $maxResults);
        $this->populate($entities);
        
        return $entities;
    }

    /**
     * Find one by critteria
     * 
     * @param type $critteria
     * @return type
     */
    public function findOneBy($critteria)
    {
        $entity = $this->repo->getOneBy($critteria);
        
        if ($entity) {
            $this->populate(array($entity));
        }
        
        return $entity;
    }
    
    public function findByActor($actor, array $orderBy = null, $limit = null, $offset = null)
    {
        $entities = $this->repo->findBy(array(
            'actor' => $actor
        ), $orderBy, $limit, $offset);
        
        $this->populate($entities);
        
        return $entities;
    }
    
    public function findByTarget($target, array $orderBy = null, $limit = null, $offset = null)
    {
        $entities = $this->repo->findBy(array(
            'target'   => $target
        ), $orderBy, $limit, $offset);
        
        $this->populate($entities);
        
        return $entities;
    }
    
    public function findByObject($object, array $orderBy = null, $limit = null, $offset = null)
    {
        $entities = $this->repo->findBy(array(
            'object'   => $object
        ), $orderBy, $limit, $offset);
        
        $this->populate($entities);
        
        return $entities;
    }
    
    public function findByIds($ids)
    {
        $entities = $this->repo->findByIds($ids);
        
        $this->populate($entities);
        
        return $entities;
    }
    
    /**
     * Fetch related DB activity entities to reduce DB queries
     * 
     * @param type $entities
     */
    public function populate($entities)
    {
        $event = new ActivityEvent(NULL, $this->em);
        $event->setEntities($entities);
        $this->eventDispatcher->dispatch(Events::ACTIVITY_POPULATE, $event);
    }
}
