<?php

namespace Gesof\ActivityStreamBundle\Services;

use Gesof\ActivityStreamBundle\Entity\Activity;

class ActivityBuilder 
{
    protected $actor;
    protected $object;
    protected $target;
    protected $verb;
    protected $code;
    protected $url;
    
    public function __construct($em)
    {

    }

    public function setActor($actor) 
    {
        $this->actor = $actor;
        
        return $this;
    }

    public function setObject($object) 
    {
        $this->object = $object;
        
        return $this;
    }

    public function setTarget($target) 
    {
        $this->target = $target;
        
        return $this;
    }

    public function setVerb($verb) 
    {
        $this->verb = $verb;
        
        return $this;
    }

    public function setCode($code) 
    {
        $this->code = $code;
        
        return $this;
    }

    public function setUrl($url) 
    {
        $this->url = $url;
        
        return $this;
    }

    public function create()
    {
        $activity = new Activity();
    }

}
