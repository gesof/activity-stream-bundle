<?php

namespace Gesof\ActivityStreamBundle\Services;

use Gesof\ActivityStreamBundle\Entity\Notification;
use Gesof\ActivityStreamBundle\Event\ActivityEvent;
use Gesof\ActivityStreamBundle\Events;

/**
 * Service: gesof_activity_stream.notification_manager
 */
class NotificationManager
{
    protected $em;
    protected $eventDispatcher;
    protected $queryParam;
    protected $repo;
    
    public function __construct($em, $eventDispatcher, $queryParam)
    {
	$this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
	$this->queryParam = $queryParam;
        $this->repo = $em->getRepository('GesofActivityStreamBundle:Notification');
    }
    
    /**
     * New instance
     */
    public function newInstance()
    {
        $entity = new Notification();
        
        return $entity;
    }
    
    /**
     * Create entity using entity or array data
     * 
     * @param type $data
     * @return Activity
     */
    public function create($data)
    {
        if ($data instanceof Notification) {
            $entity = $data;
        }
        else if (is_array($data)) {
            $entity = new Notification();
            $entity->populate($data);
        }
        else {
            throw new \Exception('Invalid data provided. Must be array or instanceof Notification');
        }
        
        $this->em->persist($entity);
        $this->em->flush($entity);
        
        $this->eventDispatcher->dispatch(Events::NOTIFICATION_CREATED, new ActivityEvent($entity, $this->em));
        
        return $entity;
    }
    
    /**
     * Find by critteria
     * 
     * @param type $critteria
     * @param type $orderBy
     * @param type $offset
     * @param type $maxResults
     * @return type
     */
    public function findBy($critteria, $orderBy = array('id' => 'DESC'), $offset = 0, $maxResults = 10)
    {
        $entities = $this->repo->getBy($critteria, $orderBy, $offset, $maxResults);
        $this->populate($entities);
        $this->addQueryParam($entities);
        
        return $entities;
    }
    
    /**
     * Find one by critteria
     * 
     * @param type $critteria
     * @return type
     */
    public function findOneBy($critteria)
    {
        $entity = $this->repo->getOneBy($critteria);
        
        if ($entity) {
            $this->populate(array($entity));
        }
        
        return $entity;
    }
    
    /**
     * Count all for user
     * 
     * @param type $user
     * @return type
     */
    public function count($user)
    {
        return $this->repo->count($user);
    }

    /**
     * Count unread for user
     * 
     * @param type $user
     * @return type
     */
    public function countUnread($user)
    {
        return $this->repo->countUnread($user);
    }
    
    /**
     * Mark entity as read
     * 
     * @param Notification $entity
     * @param type $flush
     * @return \Gesof\ActivityStreamBundle\Services\NotificationManager
     */
    public function markAsRead(Notification $entity, $flush = TRUE)
    {
        $entity
            ->setIsRead(TRUE)
            ->setReadAt(new \DateTime())
        ;
        
        if ($flush === TRUE) {
            $this->em->persist($entity);
            $this->em->flush();
        }
        
        return $this;
    }
    
    /**
     * Mark as pushed (sent by email, phone or else)
     * 
     * @param type $entities
     * @return \Gesof\ActivityStreamBundle\Services\NotificationManager
     */
    public function markAsPushed($entities)
    {
        if (!is_array($entities)) {
            $entities = array($entities);
        }
        
        foreach ($entities as $entity) {
            $entity
                ->setIsPushed(TRUE)
                ->setPushedAt(new \DateTime())
            ;
            $this->em->persist($entity);
        }

        $this->em->flush($entities);
        
        return $this;
    }
    
    /**
     * Mark entity as hidden from list
     * 
     * @param Notification $entity
     * @param type $flush
     * @return \Gesof\ActivityStreamBundle\Services\NotificationManager
     */
    public function markAsHidden(Notification $entity, $flush = TRUE)
    {
        $entity
            ->setIsHidden(TRUE)
            ->setHiddenAt(new \DateTime())
        ;
        
        if ($flush === TRUE) {
            $this->em->persist($entity);
            $this->em->flush();
        }
        
        return $this;
    }
    
    public function getForUser($user, $package = null, $offset = 0, $maxResults = 10)
    {
        $entities = $this->repo->getBy(array(
            'user' => $user,
            'package' => $package
        ), NULL, $offset, $maxResults);
	$this->populate($entities);
	$this->addQueryParam($entities);
	
	return $entities;
    }
    
    /**
     * Fetch related DB activity entities to reduce DB queries
     * 
     * @param type $entities
     */
    public function populate($entities)
    {
        $activitys = array();
        
        foreach ($entities as $entity) {
            $activitys[] = $entity->getActivity();
        }
        
        $event = new ActivityEvent(NULL, $this->em);
        $event->setEntities($activitys);
        $this->eventDispatcher->dispatch(Events::ACTIVITY_POPULATE, $event);
    }
    
    protected function addQueryParam($entities)
    {
	foreach ($entities as $entity) {
	    $url = $entity->getUrl();
	    
	    // TODO: check if internal or external URL
	    
	    if ($url) {
		$parsedUrl = parse_url($url);
		
		$queryString = isset($parsedUrl['query']) ? $parsedUrl['query'] : '';
		parse_str($queryString, $queryVars);
		
		$queryVars[$this->queryParam] = $entity->getId();
		$parsedUrl['path'] = isset($parsedUrl['path']) ? $parsedUrl['path'] : '/';
		
		$newURL = http_build_url($parsedUrl, array('query' => \http_build_query($queryVars)));
		
		if ($url !== $newURL) {
		    $entity->setUrl($newURL);
		}
	    }
	}
    }
}
