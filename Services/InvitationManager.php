<?php

namespace Gesof\ActivityStreamBundle\Services;

use Gesof\ActivityStreamBundle\Entity\Invitation;
use Gesof\ActivityStreamBundle\Event\ActivityEvent;
use Gesof\ActivityStreamBundle\Events;

class InvitationManager 
{
    protected $em;
    protected $eventDispatcher;
    
    public function __construct($em, $eventDispatcher)
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
        $this->repo = $em->getRepository('GesofActivityStreamBundle:Invitation');
    }
    
    // For custom queries
    public function getRepository()
    {
        return $this->repo;
    }
    
    /**
     * Create entity using entity or array data
     * 
     * @param type $data
     * @return Activity
     */
    public function create($data)
    {
        if ($data instanceof Invitation) {
            $entity = $data;
        }
        else if (is_array($data)) {
            $entity = new Invitation();
            $entity->populate($data);
        }
        else {
            throw new \Exception('Invalid data provided. Must be array or instanceof Invitation');
        }
        
        $this->em->persist($entity);
        $this->em->flush($entity);
        
        $this->eventDispatcher->dispatch(Events::INVITATION_CREATED, new ActivityEvent($entity, $this->em));
        
        return $entity;
    }
    
    /**
     * Find by critteria
     * 
     * @param type $critteria
     * @param type $orderBy
     * @param type $offset
     * @param type $maxResults
     * @return type
     */
    public function findBy($critteria, $orderBy = array('id' => 'DESC'), $offset = 0, $maxResults = 10)
    {
        $entities = $this->repo->getBy($critteria, $orderBy, $offset, $maxResults);
        $this->populate($entities);
        
        return $entities;
    }
    
    /**
     * Find one by critteria
     * 
     * @param type $critteria
     * @return type
     */
    public function findOneBy($critteria)
    {
        $entity = $this->repo->getOneBy($critteria);
        
        if ($entity) {
            $this->populate(array($entity));
        }
        
        return $entity;
    }
    
    /**
     * Count all for user
     * 
     * @param type $user
     * @return type
     */
    public function count($user)
    {
        return $this->repo->count($user);
    }

    /**
     * Count pending for user
     * 
     * @param type $user
     * @return type
     */
    public function countPending($user)
    {
        return $this->repo->countPending($user);
    }
    
    /**
     * Fetch related DB activity entities to reduce DB queries
     * 
     * @param type $entities
     */
    public function populate($entities)
    {
        $activitys = array();
        
        foreach ($entities as $entity) {
            $activitys[] = $entity->getActivity();
        }
        
        $event = new ActivityEvent(NULL, $this->em);
        $event->setEntities($activitys);
        $this->eventDispatcher->dispatch(Events::ACTIVITY_POPULATE, $event);
    }
}
