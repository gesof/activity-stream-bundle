<?php

namespace Gesof\ActivityStreamBundle\Serializer;

interface SerializerInterface  
{
    public function supports($object);
    public function serialize($object);
}
