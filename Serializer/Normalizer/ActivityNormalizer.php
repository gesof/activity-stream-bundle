<?php

namespace Gesof\ActivityStreamBundle\Serializer\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

use Gesof\ActivityStreamBundle\Model\ActivityInterface;

class ActivityNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    protected $templating;
    
    public function __construct($templating) 
    {
        $this->templating = $templating;
    }
    
    /** @var \Symfony\Component\Serializer\Serializer */
    public function normalize($object, $format = null, array $context = array())
    {
        $data = array(
            'id'        => $object->getId(),
            'title'     => $object->getTitle(),
            'description'=> $object->getDescription(),
            'actor'     => NULL,
            'object'    => NULL,
            'target'    => NULL,
            'url'       => NULL,
            'verb'      => $object->getVerb(),
            'code'      => $object->getCode(),
            'published' => $object->getCreatedAt() ? $object->getCreatedAt()->getTimestamp() : NULL
        );
        if ($object->getActor()) {
            $data['actor'] = $this->serializer->normalize($object->getActor(), $format, $context);
            $data['actor']['objectType'] = $object->getActorType(); 
        }
        
        if ($object->getObject()) {
            $data['object'] = $this->serializer->normalize($object->getObject(), $format, $context);
            $data['object']['objectType'] = $object->getObjectType(); 
        }
        
        if ($object->getTarget()) {
            $data['target'] = $this->serializer->normalize($object->getTarget(), $format, $context);
            $data['target']['objectType'] = $object->getTargetType(); 
        }
        
        return $data;
    }
    
    /**
     * Unsuported
     * 
     * @param type $data
     * @param type $class
     * @param type $format
     * @return type
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        return NULL;
    }
    
    public function supportsNormalization($data, $format = null)
    {
        return  $data instanceof ActivityInterface; // 'json' === $format;
    }
    
    public function supportsDenormalization($data, $type, $format = null)
    {
        return FALSE;
    }
}