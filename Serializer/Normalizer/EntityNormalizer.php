<?php

namespace Gesof\ActivityStreamBundle\Serializer\Normalizer;

use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

use Gesof\ActivityStreamBundle\Entity\Placeholder;

class EntityNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    protected $helperAssets;
    
    public function __construct($helperAssets)
    {
        $this->helperAssets = $helperAssets;
    }
    
    public function normalize($object, $format = null, array $context = array())
    {
        if ($object instanceof Placeholder) {
            $data = array(
                'id' => $object->getId(),
                'displayName' => $object->getDisplayName(),
                'objectType' => $object->getObjectType(),
                'image' => NULL,
                'url' => $object->getUrl()
            );            
        }
        else {
            $data = array(
                'id' => $object->getId(),
                'displayName' => $this->generateDisplayName($object),
                'objectType' => NULL, // $this->getObjectClass($object),
                'image' => $this->generateImageUrl($object),
                'url' => NULL
            );
        }
        
        //var_dump($this);
        
        //$this->serializer->normalize($a);
        
        return $data;
    }
    
    /**
     * Unsuported
     * 
     * @param type $data
     * @param type $class
     * @param type $format
     * @return type
     */
    public function denormalize($data, $class, $format = null)
    {
        return NULL;
    }
    
    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && method_exists($data, 'getId')? TRUE : FALSE;
    }
    
    public function supportsDenormalization($data, $type, $format = null)
    {
        return FALSE;
    }
    
    protected function generateDisplayName($object)
    {
        switch (TRUE) {
            case method_exists($object, 'getFirstname') && method_exists($object, 'getLastname'):
                $label = trim($object->getFirstname() . ' ' . $object->getLastname());
                break;
            
            case method_exists($object, 'getFullname'):
                $label = $object->getFullName();
                break;
            
            case method_exists($object, 'getName'):
                $label = $object->getName();
                break;
            
            case method_exists($object, 'getTitle'):
                $label = $object->getTitle();
                break;
            
            case method_exists($object, '__toString'):
                $label = (string) $object;
                break;
            
            default:
                $label = "";
        }
        
        $label = trim($label);
        
        if (empty($label)) {
            $label = sprintf('%s-%s', $this->getObjectClass($object), $object->getId());
        }
        
        return $label;
    }
    
    protected function generateImageUrl($object)
    {
        switch (TRUE) {
            case method_exists($object, 'getImage'):
                $imageUrl = $object->getImage() ? $this->helperAssets->getUrl($object->getImage()) : NULL;
                break;
            
            case method_exists($object, 'getImageUrl'):
                $imageUrl = $object->getImageUrl();
                break;
            
            default:
                $imageUrl = NULL;
        }
        
        return $imageUrl;
    }
    
    protected function getObjectClass($object)
    {
        $class = $this->getClass($object);
        $class = explode( '\\', $class);
        $className = end($class);

        return $className;
    }
    
    protected function getClass($object)
    {
        return is_object($object) ? ClassUtils::getClass($object) : NULL;
    }
}
