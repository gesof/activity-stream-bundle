<?php

namespace Gesof\ActivityStreamBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;

class NotificationController extends Controller
{
    public function indexAction(Request $request)
    {
        $responseData = array(
            'metadata' => array(
                'page' => 1
            ),
            'results' => array()
        );

        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            $userId = $this->getUser()->getId();
            $package  = $request->query->get('package');
            $limit = abs((int) $request->query->get('limit', 10));
            $page = abs((int) $request->query->get('page', 1));
            
            $responseData['metadata']['page'] = $page;
            $responseData['metadata']['limit'] = $limit;
            $responseData['metadata']['totalUnread'] = $em->getRepository('GesofActivityStreamBundle:Notification')->countUnread($userId);
            
            $offset = ($page - 1) * $limit;
            $maxResults = $limit;
            $entities = $em->getRepository('GesofActivityStreamBundle:Notification')->getByUser($userId, $package, $offset, $maxResults);

            $serializer = $this->get('serializer');
            $activityManager = $this->get('gesof_activity_stream.activity_manager');

            $activitys = array();

            foreach ($entities as $entity) {
                $eActivity = $entity->getActivity();
                $activitys[$eActivity->getId()] = $eActivity;
            }

            $activityManager->populate($activitys);

            $queryParam = $this->container->getParameter('gesof_activity_stream.query_param');
            
            foreach ($entities as $entity) {
                $eActivity = $entity->getActivity();
                $result = $serializer->normalize($eActivity);

                $url = $entity->getUrl() ? $entity->getUrl() : $result['url'];
                
                // Append Query param
                $result['url'] = $this->appendQueryParamsToUrl($url, array(
                    $queryParam => $entity->getId()
                ));
                
                $result['is_read'] = $entity->getIsRead();
                $result['is_pushed'] = $entity->getIsPushed();

                $responseData['results'][] = $result;
            }
        }
        
        return new JsonResponse($responseData);
    }
    
    // In development
    public function markActivity(Request $request, $state)
    {
        
    }
    
    protected function appendQueryParamsToUrl($url, $params)
    {
        $parsedUrl = parse_url($url);

        $query = isset($parsedUrl['query']) ? $parsedUrl['query'] : '';
        parse_str($query, $parsedParams);
        
        foreach ($params as $param => $value) {
            $parsedParams[$param] = $value;
        }
        
        $parsedUrl['query'] = http_build_query($parsedParams);

        $computedUrl = http_build_url($parsedUrl);

        return $computedUrl;
    }
}
