<?php

namespace Gesof\ActivityStreamBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{
    public function indexAction()
    {
        if (!$this->get('security.context')->isGranted('ROLE_PROGRAMMER')) {
            return new Response('Forbidden', 403);
        }

//        $this->testCreate();
        return $this->test();
        
        return $this->render('GesofActivityStreamBundle:Test:index.html.twig', array());
    }
    
    protected function test()
    {
        $event = new \Gesof\ActivityStreamBundle\Event\ActivityEvent();
        
       // $this->get('event_dispatcher')->dispatch(\Gesof\ActivityStreamBundle\Events::ACTIVITY_CREATE, $event);
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('AppBundle:User')->find(1);
        
        $activityManager = $this->get('gesof_activity_stream.activity_manager');
        $activity = $activityManager->newInstance();
        $activity
            ->setActor($user, 'person')
            ->setVerb('post')
        ;
        
        $activityManager->create($activity);
        
        return new Response('done');
    }
    
    protected function testFind()
    {
        $em = $this->getDoctrine()->getManager();
        
        $manager = $this->get('gesof_activity_stream.activity_manager');
        $notificationManager = $this->get('gesof_activity_stream.notification_manager');
        $invitationManager = $this->get('gesof_activity_stream.invitation_manager');
        
        $eMicro = $em->getRepository('GiaMacroPortalBundle:MicroPortal')->find(1);
        
        //$entries = $manager->findByObject($eMicro);
        $entries = $manager->findBy(array(
            'entity' => $eMicro
        ));
        
        $invitations = $invitationManager->findBy(array(
            'user' => $this->getUser(),
            'entity' => $eMicro
        ));
        $notifications = $notificationManager->findBy(array(
            'user' => $this->getUser(),
            'entity' => $eMicro
        ));
        
        foreach ($notifications as $n) {
            echo "nid: " . $n->getId() . '<br />';
        }
        
        $serializerManager = $this->get('gesof_activity_stream.serializer.manager');
        
        $items = array(
            
        );
        
        foreach ($entries as $entry) {
            $activity = $serializerManager->serialize($entry);
            //$activity['content'] = $content;
            
            $items[] = array(
                'activity' => $activity,
                'notification' => array(
                    'url' => 'http://gesof.lh/app_dev.php/notification/3/show'
                )
            );
        }
        
        return new \Symfony\Component\HttpFoundation\JsonResponse($items);
    }
    
    protected function testCreate()
    {
        $eventDispatcher = $this->get('event_dispatcher');
        $eventDispatcher->addListener(\Gesof\ActivityStreamBundle\Events::ACTIVITY_CREATE, function ($event) {
            echo "created activity stream (Activity) with id: " . $event->getEntity()->getId();
        });
        
        $manager = $this->get('gesof_activity_stream.activity_manager');
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->findBy(array(), null, 2);
        $eMicro = $em->getRepository('GiaMacroPortalBundle:MicroPortal')->find(1);
        
        $manager->create('like', $users[0], $users[1], $eMicro);
    }
}
