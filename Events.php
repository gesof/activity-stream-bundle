<?php

namespace Gesof\ActivityStreamBundle;

final class Events 
{
     // Activity entity has been created
     const ACTIVITY_CREATE = 'gesof_activity_stream.activity.create';
     const ACTIVITY_POPULATE = 'gesof_activity_stream.activity.populate';
     const INVITATION_CREATED = 'gesof_activity_stream.invitation.created';
     const NOTIFICATION_CREATED = 'gesof_activity_stream.notification.created';
     const NOTIFICATION_MARK_AS_READ = 'gesof_activity_stream.notification.read';
}
