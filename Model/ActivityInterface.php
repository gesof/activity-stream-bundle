<?php

namespace Gesof\ActivityStreamBundle\Model;

interface ActivityInterface 
{
    public function getVerb();
    public function getUrl();
    public function getCode();
    public function getActor();
    public function getTarget();
    public function getObject();
}
