<?php

namespace Gesof\ActivityStreamBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ActivityEvent extends Event
{
    protected $entities;
    protected $entity;
    protected $em;
    
    public function __construct($entity = NULL, $em = NULL)
    {
        $this->entity = $entity;
        $this->em = $em;
    }
    
    public function getEntity()
    {
        return $this->entity;
    }
    
    public function getEntityManager()
    {
        return $this->em;
    }
    
    public function getEntities() 
    {
        return $this->entities;
    }

    public function setEntities($entities) 
    {
        $this->entities = $entities;
    }


}
