<?php

namespace Gesof\ActivityStreamBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class NotificationEvent extends Event
{
    protected $user;
    protected $notification;
    protected $id;
    
    public function __construct()
    {

    }
    
    public function getUser()
    {
	return $this->user;
    }

    public function setUser($user)
    {
	$this->user = $user;
	
	return $this;
    }
    
    public function getNotification()
    {
	return $this->notification;
    }

    public function setNotification($notification)
    {
	$this->notification = $notification;
	
	return $this;
    }

    public function getId()
    {
	return $this->id;
    }

    public function setId($id)
    {
	$this->id = $id;
	
	return $this;
    }
}
