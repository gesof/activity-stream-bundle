branch: dev-2.1

```
# add to composer on repositories
        {
            "type": "vcs",
            "url": "ssh://hg@bitbucket.org/gesof/activity-stream-bundle"
        }
```

```
# run in console
php composer.phar require "gesof/activity-stream-bundle"
```

```
# AppKernel.php
new Gesof\ActivityStreamBundle\GesofActivityStreamBundle(),
```

```
# config.yml
gesof_activity_stream:
    user_class: AppBundle\Entity\User
    # optional
    query_param: _notif

...
doctrine:
    orm:
        entity_managers:
            default:
                mappings:
                    ...
                    GesofActivityStreamBundle
```

```
# routing.yml
gesof_activity_stream:
    resource: "@GesofActivityStreamBundle/Resources/config/routing.yml"
    prefix:   /
```
```
# usage
        $activityStreamManager = $this->get('gesof_activity_stream.activity_manager');
        
       $activity = $activityStreamManager->newInstance();
       $activity
           ->setActor($actorEntity)
           ->setObject($objectEntity)
           ->setTarget($targetEntity)
           ->setVerb($verb)
           ->setCode($code)
           ->setUrl($activityUrl)
       ;
       $activityStreamManager->create($activity);
```

```
# testing
php bin/phpunit --configuration app/phpunit.xml src/Gesof/ActivityStreamBundle/Tests
```

based on
http://knpbundles.com/redpanda/ActivityStreamBundle
https://github.com/redpanda/ActivityStreamBundle
an extension of
https://github.com/redpanda/ActivityStreams

http://activitystrea.ms/registry/verbs/
https://developer.zendesk.com/rest_api/docs/core/dynamic_content

https://developer.atlassian.com/docs/atlassian-platform-common-components/activity-streams

about google activity stream
http://www.ibm.com/developerworks/library/x-googleplusphp/

see json response
https://crowdin.com/project/reddit/activity_stream
http://activitystrea.ms/specs/json/1.0/#media-link

hosted services
http://wiki.activitystrea.ms/w/page/24500522/Implementors
    Cliqset 
    Facebook 
    Gnip 
    Google Buzz 
    Gowalla 
    Hulu 
    MySpace
    Opera
    Socialcast
    Status.net
    Superfeedr 
    TypePad 
    Windows Live 
    Yiid 
    ... 

Please be aware that there are generally three different implementation methods to providing ActivityStreams: polling, push-based, and real-time/streaming.
http://wiki.activitystrea.ms/w/page/19394614/Implementation-Scenarios

important PDF
http://wiki.activitystrea.ms/w/file/fetch/23276107/G7_actionstreams_2-18-09.pdf

UML SCHEMA
http://xmlns.notu.be/aair/activity_stream_UML.png

examples
http://documentation.ektron.com/cms400/v802/WebHelp/Managing%20User%20Communities/Activity%20Streams/Community_Management_ActivityStreams.htm#Sharing

read also
https://github.com/ckan/ckan/wiki/Spec:-Activity-Stream-Notifications
https://www.exoplatform.com/docs/public/index.jsp?topic=%2FPLF41%2FPLFUserGuide.ManagingYourPersonalApplications.ManagingEmailNotification.PostOnYourActivityStream.html
https://www.drupal.org/node/1710688
http://sales.hubspot.com/knowledge/receiving-notifications

agregates, grouping notifications
http://feedly.readthedocs.org/en/latest/notification_systems.html
http://feedly.readthedocs.org/en/latest/stream_framework.verbs.html

read
https://apps.na.collabserv.com/help/index.jsp?topic=%2Fcom.ibm.cloud.welcome.doc%2Factivitystream.html


serializing objects:
http://activitystrea.ms/specs/json/1.0/#object
