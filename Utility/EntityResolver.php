<?php

namespace Gesof\ActivityStreamBundle\Utility;

class EntityResolver 
{
    protected $em;
    protected $uow;
    protected $map = array();
    protected $queue = array();
    
    public function __construct($em)
    {
        $this->em = $em;
        $this->uow = $em->getUnitOfWork();
    }
    
    /**
     * Add to queue for select
     * 
     * @param type $id
     * @param type $className
     * @return \Gesof\ActivityStreamBundle\Services\EntityResolver
     */
    public function queue($id, $className)
    {
        $rootClassName = $this->getRootClassName($className);
        
        if ($rootClassName && !(isset($this->queue[$className][$id]) || isset($this->map[$className][$id]))) {
            // Check in identity map
            $existing = $this->uow->tryGetByIdHash($id, $rootClassName);
            
            // Found in identity map
            if ($existing) {
                $this->map[$className][$id] = $existing;
            }
            // Add to queue
            else {
                $this->queue[$className][$id] = $id;
            }
        }
        
        return $this;
    }
    
    /**
     * Get entity
     * 
     * @param type $id
     * @param type $className
     * @return type
     */
    public function get($id, $className)
    {
        $rootClassName = $this->getRootClassName($className);
        
        if ($rootClassName) {
            // Trigger process if queue not empty
            if (count($this->queue) > 0) {
                $this->resolve();
            }
        }
        
        return isset($this->map[$className][$id]) ? $this->map[$className][$id] : NULL;
    }
    
    /**
     * Resolve root class name (mapped)
     * 
     * @param type $className
     * @return type
     */
    public function getRootClassName($className)
    {
        try {
            $rootClassName = $this->em->getClassMetadata($className)->rootEntityName;
        }
        catch (\Exception $e) {
            $rootClassName = NULL;
        }
        
        return $rootClassName;
    }
    
    public function resolve()
    {
        foreach ($this->queue as $className => $ids) {
            $qb = $this->em->getRepository($className)->createQueryBuilder('e');
            $qb
                ->select('e')
                ->where($qb->expr()->in('e.id', $ids))
            ;
            
            $entities = $qb->getQuery()->getResult();
            
            // Populating map
            foreach ($entities as $entity) {
                $this->map[$className][$entity->getId()] = $entity;
            }
        }
        
        $this->queue = array();
        
        return $this;
    }
}
