<?php

namespace Gesof\ActivityStreamBundle\EventListener\Custom;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Gesof\ActivityStreamBundle\Event\ActivityEvent;
use Gesof\ActivityStreamBundle\Event\NotificationEvent;
use Gesof\ActivityStreamBundle\Events;
use Gesof\ActivityStreamBundle\Utility\EntityResolver;

class NotificationSubscriber implements EventSubscriberInterface
{
    protected $em;
    
    public function __construct($em)
    {
	$this->em = $em;
    }
    
    public static function getSubscribedEvents()
    {
        return array(
            Events::NOTIFICATION_MARK_AS_READ => 'onMarkAsRead',
            Events::ACTIVITY_POPULATE => 'onActivityPopulate'
        );
    }
    
    public function onMarkAsRead(NotificationEvent $event)
    {
	$notification = $this->em->getRepository('GesofActivityStreamBundle:Notification')->find($event->getId());
	
	if ($notification && $notification->getIsRead() !== TRUE) {
            $notification
                ->setIsRead(TRUE)
                ->setReadAt(new \DateTime())
            ;
            
            $this->em->persist($notification);
            $this->em->flush();
	}
    }
    
    public function onActivityPopulate(ActivityEvent $event)
    {
        $em = $event->getEntityManager();
        $entities = $event->getEntities();
        $entityResolver = new EntityResolver($em);
        
        foreach ($entities as $entry) {
            $entityResolver
                ->queue($entry->getActorId(), $entry->getActorClass())
                ->queue($entry->getTargetId(), $entry->getTargetClass())
                ->queue($entry->getObjectId(), $entry->getObjectClass())
            ;
        }
        
        $entityResolver->resolve();
            
        foreach ($entities as $entry) {
            $entry
                ->setActor($entityResolver->get($entry->getActorId(), $entry->getActorClass()))
                ->setTarget($entityResolver->get($entry->getTargetId(), $entry->getTargetClass()))
                ->setObject($entityResolver->get($entry->getObjectId(), $entry->getObjectClass()))
            ;
        }
    }
}