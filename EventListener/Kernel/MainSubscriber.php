<?php

namespace Gesof\ActivityStreamBundle\EventListener\Kernel;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
//use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
//use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

use Gesof\ActivityStreamBundle\Event\NotificationEvent;
use Gesof\ActivityStreamBundle\Events;

class MainSubscriber implements EventSubscriberInterface
{
    protected $securityContext;
    protected $eventDispatcher;
    protected $queryParam;
    
    public function __construct($securityContext, $eventDispatcher, $queryParam)
    {
	$this->securityContext = $securityContext;
	$this->eventDispatcher = $eventDispatcher;
	$this->queryParam = $queryParam;
    }
    
    public static function getSubscribedEvents()
    {
	return array(
	    'kernel.request' => 'onKernelRequest'
	);
    }
    
    public function onKernelRequest(GetResponseEvent $event)
    {
	// Not a master request
        if (!$event->isMasterRequest()) {
            return;
        }
	
	$request = $event->getRequest();
	
	// Hash detected
	if ($request->query->has($this->queryParam)) {
	    $notificationId = $request->query->get($this->queryParam);
	
	    if ($notificationId && $this->getUser()) {
		$notificationEvent = new NotificationEvent();
		$notificationEvent
		    ->setUser($this->getUser())
		    ->setId($notificationId)
		;

		$this->eventDispatcher->dispatch(Events::NOTIFICATION_MARK_AS_READ, $notificationEvent);
	    }
	}
    }
    
    protected function getUser()
    {
        if (null === $token = $this->securityContext->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            return;
        }

        return $user;
    }
}
