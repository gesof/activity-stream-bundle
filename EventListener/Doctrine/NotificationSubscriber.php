<?php

namespace Gesof\ActivityStreamBundle\EventListener\Doctrine;

use Doctrine\Common\EventSubscriber;
// use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\Persistence\Event\LoadClassMetadataEventArgs;

// use Gesof\NotificationBundle\Entity\NotificationToUser;

class NotificationSubscriber implements EventSubscriber
{
    protected $userClass;
    
    public function __construct($userClass)
    {
	$this->userClass = $userClass;
    }
    
    public function getSubscribedEvents()
    {
        return array(
	    'loadClassMetadata'
        );
    }
    
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
	/* @var $metadata \Doctrine\ORM\Mapping\ClassMetadata */
	$metadata = $eventArgs->getClassMetadata();
	
	if ($metadata->getName() != 'Gesof\ActivityStreamBundle\Entity\Notification') {
            return;
        }
	       
//        $namingStrategy = $eventArgs
//            ->getEntityManager()
//            ->getConfiguration()
//            ->getNamingStrategy()
//        ;
	
	$metadata->mapManyToOne(array(
            'targetEntity'  => $this->userClass,
            'fieldName'     => 'User',
	    'cascade' => array('persist'),
            'joinColumns'     => array(
		array(
		    'name'        => 'user_id',
		    'referencedColumnName' => 'id',
		    'nullable' => FALSE,
		    'onDelete' => 'CASCADE'
		)
            )
        ));
	
    }
}
