<?php

namespace Gesof\ActivityStreamBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ActivityRepository extends EntityRepository
{
    public function getBy($critteria, $orderBy = array('id' => 'DESC'), $offset = 0, $maxResults = 10)
    {
        return $this->getByQuery($critteria, $orderBy, $offset, $maxResults)->getResult();
    }

    public function getByQuery($critteria, $orderBy, $offset, $maxResults)
    {
        return $this->getByQB($critteria, $orderBy, $offset, $maxResults)->getQuery();
    }
    
    public function getByQB($critteria, $orderBy, $offset, $maxResults) 
    {
	/* @var $qb \Doctrine\ORM\QueryBuilder */
	$qb = $this->createQueryBuilder('act');
	$qb
            ->select('act')
        ;
        
        $this->populateQB($qb, $critteria);

        // Order by
        foreach ($orderBy as $field => $direction) {
            $qb
                ->addOrderBy('act.' . $field, $direction)
            ;
        }
        
        // Limit
        $qb
            ->setFirstResult($offset)
            ->setMaxResults($maxResults)
        ;  
        
        // Id required
        if (isset($critteria['id'])) {
            $qb
                ->andWhere('act.id = :id')
                ->setParameter('id', $critteria['id'])
            ;            
        }

        return $qb;
    }
    
    // --
    
    public function getOneBy($critteria)
    {
        return $this->getOneByQuery($critteria)->getOneOrNullResult();
    }

    public function getOneByQuery($critteria)
    {
        return $this->getOneByQB($critteria)->getQuery();
    }
    
    public function getOneByQB($critteria) 
    {
	/* @var $qb \Doctrine\ORM\QueryBuilder */
	$qb = $this->createQueryBuilder('act');
	$qb
            ->select('act')
        ;
        
        $this->populateQB($qb, $critteria);
        
        // Limit
        $qb
            ->setMaxResults(1)
        ;  
        
        // Id required
        if (isset($critteria['id'])) {
            $qb
                ->andWhere('act.id = :id')
                ->setParameter('id', $critteria['id'])
            ;            
        }
        
        return $qb;
    }
    
    // --
    
    public function populateQB($qb, $critteria)
    {
        // Extract entity, actor, object or target essentials
        if (isset($critteria['entity'])) {
            $critteria['entity_id'] = $critteria['entity']->getId();
            $critteria['entity_type'] = get_class($critteria['entity']);
        }
        
        if (isset($critteria['actor'])) {
            $critteria['actor_id'] = $critteria['actor']->getId();
            $critteria['actor_type'] = get_class($critteria['actor']);
        }
        
        if (isset($critteria['object'])) {
            $critteria['object_id'] = $critteria['object']->getId();
            $critteria['object_type'] = get_class($critteria['object']);
        }
        
        if (isset($critteria['target'])) {
            $critteria['target_id'] = $critteria['target']->getId();
            $critteria['target_type'] = get_class($critteria['target']);
        }
        
        // Handle OR
        // Entity required in either actor, object or target
        if (isset($critteria['entity_type']) && isset($critteria['entity_id'])) {
            $qbActor = $qb->expr()->andX($qb->expr()->eq('act.actor_type', ':entity_type'), $qb->expr()->eq('act.actor_id', ':entity_id'));
            $qbObject = $qb->expr()->andX($qb->expr()->eq('act.object_type', ':entity_type'), $qb->expr()->eq('act.object_id', ':entity_id'));
            $qbTarget = $qb->expr()->andX($qb->expr()->eq('act.target_type', ':entity_type'), $qb->expr()->eq('act.target_id', ':entity_id'));
            
            $qb
                ->setParameter('entity_type', $critteria['entity_type'])
                ->setParameter('entity_id', $critteria['entity_id'])
            ;
            
            $qbEntity = $qb->expr()->orX($qbActor, $qbObject, $qbTarget);
            $qb->andWhere($qbEntity);
        }

        // Handle AND
        $qb1 = $qb->expr()->andX();
        
        // Actor required
        if (isset($critteria['actor_type']) && isset($critteria['actor_id'])) {
            $qbActor = $qb->expr()->andX($qb->expr()->eq('act.actor_type', ':actor_type'), $qb->expr()->eq('act.actor_id', ':actor_id'));
            
            $qb
                ->setParameter('actor_type', $critteria['actor_type'])
                ->setParameter('actor_id', $critteria['actor_id'])
            ;
            
            $qb1->add($qbActor);
        }

        // Object required
        if (isset($critteria['object_type']) && isset($critteria['object_id'])) {
            $qbObject = $qb->expr()->andX($qb->expr()->eq('act.object_type', ':object_type'), $qb->expr()->eq('act.object_id', ':object_id'));
            
            $qb
                ->setParameter('object_type', $critteria['object_type'])
                ->setParameter('object_id', $critteria['object_id'])
            ;
            
            $qb1->add($qbObject);
        }
        
        // Target required
        if (isset($critteria['target_type']) && isset($critteria['target_id'])) {
            $qbTarget = $qb->expr()->andX($qb->expr()->eq('act.target_type', ':target_type'), $qb->expr()->eq('act.target_id', ':target_id'));
            
            $qb
                ->setParameter('target_type', $critteria['target_type'])
                ->setParameter('target_id', $critteria['target_id'])
            ;
            
            $qb1->add($qbTarget);
        }
                
        if ($qb1->count() > 0) {
            $qb->andWhere($qb1);
        }
        
        // Verb required
        if (isset($critteria['verb'])) {
            $qb
                ->andWhere($qb->expr()->eq('act.verb', ':verb'))
                ->setParameter('verb', $critteria['verb'])
            ;
        }
        
        // Code required
        if (isset($critteria['code'])) {
            $qb
                ->andWhere($qb->expr()->eq('act.code', ':code'))
                ->setParameter('code', $critteria['code'])
            ;
        }
        
        // Package required
        if (isset($critteria['package'])) {
            $qb
                ->andWhere($qb->expr()->eq('act.package', ':package'))
                ->setParameter('package', $critteria['package'])
            ;
        }
        
        // Period start required
        if (isset($critteria['since'])) {
            $qb
                ->andWhere('act.created_at >= :since')
                ->setParameter('since', $critteria['since'])
            ;
        }
        
        // Period end required
        if (isset($critteria['until'])) {
            $qb
                ->andWhere('act.created_at <= :until')
                ->setParameter('until', $critteria['until'])
            ;
        }
    }
}
