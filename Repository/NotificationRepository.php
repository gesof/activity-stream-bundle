<?php

namespace Gesof\ActivityStreamBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NotificationRepository extends EntityRepository
{
    public function createQB()
    {
        return $this->createQueryBuilder('ntf');
    }
    
    /**
     * QB helper: is hidden
     * 
     * @param type $qb
     * @param type $isHidden
     * @return \Gesof\ActivityStreamBundle\Repository\NotificationRepository
     */
    public function addWhereIsHidden($qb)
    {
        $qb
            ->andWhere('ntf.is_hidden = :notification_is_hidden')
            ->setParameter('notification_is_hidden', TRUE)
        ;
                
        return $this;
    }
    
    /**
     * QB helper: is NOT hidden
     * 
     * @param type $qb
     * @return \Gesof\ActivityStreamBundle\Repository\NotificationRepository
     */
    public function addWhereIsNotHidden($qb)
    {
        $qb
            ->andWhere('ntf.is_hidden = :notification_is_not_hidden OR ntf.is_hidden IS NULL')
            ->setParameter('notification_is_not_hidden', FALSE)
        ;
                
        return $this;
    }
    
    public function getBy($critteria, $orderBy = array('id' => 'DESC'), $offset = 0, $maxResults = 10)
    {
        return $this->getByQuery($critteria, $orderBy, $offset, $maxResults)->getResult();
    }

    public function getByQuery($critteria, $orderBy, $offset, $maxResults)
    {
        return $this->getByQB($critteria, $orderBy, $offset, $maxResults)->getQuery();
    }
    
    public function getByQB($critteria, $orderBy, $offset, $maxResults) 
    {
        $em = $this->getEntityManager();
	$qb = $this->createQueryBuilder('ntf');
	$qb
	    ->select('ntf', 'act', 'u')
            ->leftJoin('ntf.Activity', 'act')
            ->leftJoin('ntf.User', 'u')
        ;
        
        // By user
        if (isset($critteria['user'])) {
            $qb
                ->andWhere('u.id = :user')
                ->setParameter('user', $critteria['user'])
            ;
        }
        
        $em->getRepository('GesofActivityStreamBundle:Activity')->populateQB($qb, $critteria);
        
        // Is read or not
        if (isset($critteria['is_read'])) {
            $qb
                ->andWhere('ntf.is_read = :is_read')
                ->setParameter('is_read', $critteria['is_read'])
            ;            
        }
        
        // Is published or not
        if (isset($critteria['is_pushed'])) {
            $qb
                ->andWhere('ntf.is_pushed = :is_pushed')
                ->setParameter('is_pushed', $critteria['is_pushed'])
            ;
        }

        // Order by
        foreach ($orderBy as $field => $direction) {
            $qb
                ->addOrderBy('act.' . $field, $direction)
            ;
        }
        
        // Limit
        $qb
            ->setFirstResult($offset)
            ->setMaxResults($maxResults)
        ;  
        
        // Id required
        if (isset($critteria['id'])) {
            $qb
                ->andWhere('ntf.id = :id')
                ->setParameter('id', $critteria['id'])
            ;            
        }
        
        return $qb;
    }
    
    // --
    
    public function getOneBy($critteria)
    {
        return $this->getOneByQuery($critteria)->getOneOrNullResult();
    }

    public function getOneByQuery($critteria)
    {
        return $this->getOneByQB($critteria)->getQuery();
    }
    
    public function getOneByQB($critteria) 
    {
        $em = $this->getEntityManager();
	$qb = $this->createQueryBuilder('ntf');
	$qb
	    ->select('ntf', 'act', 'u')
            ->leftJoin('ntf.Activity', 'act')
            ->leftJoin('ntf.User', 'u')
        ;
        
        // By user
        if (isset($critteria['user'])) {
            $qb
                ->andWhere('u.id = :user')
                ->setParameter('user', $critteria['user'])
            ;
        }
        
        $em->getRepository('GesofActivityStreamBundle:Activity')->populateQB($qb, $critteria);
        
        // Is read or not
        if (isset($critteria['is_read'])) {
            $qb
                ->andWhere('ntf.is_read = :is_read')
                ->setParameter('is_read', $critteria['is_read'])
            ;            
        }
        
        // Is published or not
        if (isset($critteria['is_pushed'])) {
            $qb
                ->andWhere('ntf.is_pushed = :is_pushed')
                ->setParameter('is_pushed', $critteria['is_pushed'])
            ;
        }
        
        // Status
        if (isset($critteria['status'])) {
            $qb
                ->andWhere('ntf.is_read = :is_read')
                ->setParameter('is_read', $critteria['is_read'])
            ;            
        }
        
        // Limit
        $qb
            ->setMaxResults(1)
        ;  
        
        // Id required
        if (isset($critteria['id'])) {
            $qb
                ->andWhere('ntf.id = :id')
                ->setParameter('id', $critteria['id'])
            ;            
        }
        
        return $qb;
    }
    
    // --
    
    public function getByUserQB($user, $package, $offset, $maxResults)
    {
	$qb = $this->createQueryBuilder('ntf');
	$qb
	    ->select('ntf', 'act')
            ->leftJoin('ntf.Activity', 'act')
	    ->leftJoin('ntf.User', 'u')
	    ->where('u.id = :user')
	    ->setParameter('user', $user)
	    ->setFirstResult($offset)
	    ->setMaxResults($maxResults)
            ->orderBy('ntf.created_at', 'DESC')
	;
	
        if ($package) {
            $qb 
                ->andWhere('act.package = :package')
                ->setParameter('package', $package)
            ;
        }
        else {
            $qb 
                ->andWhere('act.package IS NULL')
            ;
        }
        
        return $qb;
    }
    
    public function getByUserQuery($user, $package, $offset, $maxResults)
    {
        return $this->getByUserQB($user, $package, $offset, $maxResults)->getQuery();
    }

    public function getByUser($user, $package, $offset, $maxResults)
    {
        return $this->getByUserQuery($user, $package, $offset, $maxResults)->getResult();
    }

    public function count($user)
    {
	$qb = $this->createQueryBuilder('ntf');
	$qb
	    ->select('COUNT(ntf.id) AS total')
	    ->leftJoin('ntf.User', 'u')
	    ->where('u.id = :user')
	    ->setParameter('user', $user)
	;
        
        $total = $qb->getQuery()->getSingleScalarResult();
        
        return $total;
    }
    
    public function countUnread($user)
    {
	$qb = $this->createQueryBuilder('ntf');
	$qb
	    ->select('COUNT(ntf.id) AS total')
	    ->leftJoin('ntf.User', 'u')
	    ->where('u.id = :user')
            ->andWhere('ntf.is_read = :is_read')
	    ->setParameter('user', $user)
            ->setParameter('is_read', FALSE)
	;
        
        $total = $qb->getQuery()->getSingleScalarResult();
        
        return $total;
    }
    // --

    public function markAsRead($notification)
    {
	$now = new \DateTime();
	
	$qb = $this->createQueryBuilder('ntf');
	$qb
	    ->update('GesofActivityStreamBundle:Notification', 'ntf')
	    ->set('ntf.is_read', ':is_read')
	    ->set('ntf.read_at', ':read_at')
	    ->where('ntf = :notification')
	    ->setParameter('is_read', TRUE)
	    ->setParameter('read_at', $now)
	    ->setParameter('notification', $notification)
	;

	$qb->getQuery()->execute();
    }
}
