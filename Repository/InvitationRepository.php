<?php

namespace Gesof\ActivityStreamBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Gesof\ActivityStreamBundle\Entity\Invitation;

class InvitationRepository extends EntityRepository
{
    // Find
    public function getBy($critteria, $orderBy = array('id' => 'DESC'), $offset = 0, $maxResults = 10)
    {
        return $this->getByQuery($critteria, $orderBy, $offset, $maxResults)->getResult();
    }

    public function getByQuery($critteria, $orderBy, $offset, $maxResults)
    {
        return $this->getByQB($critteria, $orderBy, $offset, $maxResults)->getQuery();
    }
    
    public function getByQB($critteria, $orderBy, $offset, $maxResults) 
    {
        $em = $this->getEntityManager();
	$qb = $this->createQueryBuilder('inv');
	$qb
	    ->select('inv', 'act')
            ->leftJoin('inv.Activity', 'act')
        ;
        
        $em->getRepository('GesofActivityStreamBundle:Activity')->populateQB($qb, $critteria);
        
        // Status
        if (isset($critteria['status'])) {
            $qb
                ->andWhere('ntf.is_read = :is_read')
                ->setParameter('is_read', $critteria['is_read'])
            ;            
        }

        // Order by
        foreach ($orderBy as $field => $direction) {
            $qb
                ->addOrderBy('act.' . $field, $direction)
            ;
        }
        
        // Limit
        $qb
            ->setFirstResult($offset)
            ->setMaxResults($maxResults)
        ;  
        
        // Id required
        if (isset($critteria['id'])) {
            $qb
                ->andWhere('inv.id = :id')
                ->setParameter('id', $critteria['id'])
            ;            
        }
        
        return $qb;
    }
    
    // --
    
    public function getOneBy($critteria)
    {
        return $this->getOneByQuery($critteria)->getOneOrNullResult();
    }

    public function getOneByQuery($critteria)
    {
        return $this->getOneByQB($critteria)->getQuery();
    }
    
    public function getOneByQB($critteria) 
    {
        $em = $this->getEntityManager();
	$qb = $this->createQueryBuilder('inv');
	$qb
	    ->select('inv', 'act')
            ->leftJoin('inv.Activity', 'act')
        ;
        
        $em->getRepository('GesofActivityStreamBundle:Activity')->populateQB($qb, $critteria);
        
        // Status
        if (isset($critteria['status'])) {
            $qb
                ->andWhere('ntf.is_read = :is_read')
                ->setParameter('is_read', $critteria['is_read'])
            ;            
        }
        
        // Limit
        $qb
            ->setMaxResults(1)
        ;  
        
        // Id required
        if (isset($critteria['id'])) {
            $qb
                ->andWhere('inv.id = :id')
                ->setParameter('id', $critteria['id'])
            ;            
        }
        
        return $qb;
    }
    
    /**
     * Count all for object
     * 
     * @param type $object
     * @return type
     */
    public function count($object)
    {
	$qb = $this->countQB($object);
        
        $total = $qb->getQuery()->getSingleScalarResult();
        
        return $total;
    }
    
    /**
     * Count pending for object
     * 
     * @param type $object
     * @return type
     */
    public function countPending($object)
    {
        $qb = $this->countQB($object);
        $qb
            ->andWhere('inv.status = :status_pending')
            ->setParameter('status_pending', Invitation::STATUS_PENDING)
	;
        
        $total = $qb->getQuery()->getSingleScalarResult();
        
        return $total;
    }
    
    public function countQB($object)
    {
        $qb = $this->createQueryBuilder('inv');
	$qb
	    ->select('COUNT(inv.id) AS total')
	    ->leftJoin('inv.Activity', 'act')
	    ->where('act.object_id = :object_id')
            ->andWhere('act.object_type = :object_type')
	    ->setParameter('object_id', $object->getId())
            ->setParameter('object_type', get_class($object))
        ;
        
        return $qb;
    }
}
